#include<stdio.h>
#include<stdlib.h>

#define DICT_SIZE 15
#define WORD_LEN 10
#define LINE_LEN 18

char Random_char();
void Game_Board(char *dict[DICT_SIZE],int coord[][4]);
void print_table(char Game_Board[][15]);
void Put_the_words(char Game_Board[][15], char *dict[DICT_SIZE],int coord[][4]);
void Set_Vertical (char Game_Board[][15],char *dict, int x,int y,int cnst,int flag);
void Set_Diagonal (char Game_Board[][15],char *dict, int x1,int y1, int x2, int y2,int flag);
void Set_Horizontal (char Game_Board[][15],char *dict, int x,int y,int cnst,int flag);
void Till_the_End (char Game_Board[][15],char *dict[DICT_SIZE],int coord[][4]);
int get_line_size(char *line) {
	char *ch_iter = line; // so as not to lose beginning of line
	int counter = 0;
	// go until you see new line or null char
	while(*ch_iter != '\n' && *ch_iter != '\0') {
		ch_iter++; // next char
		counter++; // increment counter
	}
	
	return counter;
}

void copy_string(char *source, char *destination) {
	// get iterators over original pointers
	char *src_iter = source;
	char *dst_iter = destination;
	// until null char
	while (*src_iter != '\0') {
		// copy pointers
		*dst_iter = *src_iter;
		// advance to next char
		src_iter++;
		dst_iter++;
   }
   // terminate string
   *dst_iter = '\0';
}

void remove_newline(char *line) {
	char *ch_iter = line;
	// go until you see new line
	while(*ch_iter != '\n') {
		ch_iter++; // next char
	}
	*ch_iter = '\0'; // overwrite new line
}

void print_dictionary(char *dict[]) {
	int i;
	for(i = 0 ; i < DICT_SIZE ; i++) {
		//printf("%s\n", dict[i]);
	}
}

void print_coord(int coord[DICT_SIZE][4]) {
	int i, j;
	for(i = 0 ; i < DICT_SIZE ; i++) {
		for(j = 0 ; j < 4 ; j++) {
			//printf("%d ", coord[i][j]);
		}
		//printf("\n");
	}
}

int main(){
	char *dict[DICT_SIZE];
    int coord[DICT_SIZE][4];    
    char line[LINE_LEN];
	FILE *fp = fopen("word_hunter.dat", "r");
	
	int line_counter = 0;
	int dict_counter = 0;
	while(fgets(line, LINE_LEN, fp) != NULL) {
		if(line_counter%5 == 0) {
			dict[dict_counter] = (char*) malloc(sizeof(char) * get_line_size(line));
			remove_newline(line);
			copy_string(line, dict[dict_counter]);
		} else if (line_counter%5 == 1){
			coord[dict_counter][0] = atoi(line);
		} else if (line_counter%5 == 2){			
			coord[dict_counter][1] = atoi(line);		
		} else if (line_counter%5 == 3){
			coord[dict_counter][2] = atoi(line);
		} else if (line_counter%5 == 4){
			coord[dict_counter][3] = atoi(line);
			dict_counter++;
		}
		line_counter++;
	}
	
	fclose(fp);
	print_dictionary(dict);
	print_coord(coord);
	
	// WRITE HERE...

	Game_Board(dict,coord);
	return 0;
}

void Game_Board(char *dict[DICT_SIZE],int coord[][4]){  //doing the board

	char Game_Board[15][15];
	int i,j;
	for(i=0;i<15;i++){
		for(j=0;j<15;j++){
			Game_Board[i][j] = Random_char();     // call Random_char function to make random chars


		


		}
	}
	//print_table(Game_Board);
	Put_the_words(Game_Board,dict,coord);		// after that i put the words that read the file.




}

char Random_char(){								// ****************THIS IS RANDOM CHAR FUNCTION*******************

	 char randomletter = 'a' + (random() % 26);

	 return randomletter;
}

void print_table(char Game_Board[][15]){		//printing the table in this function.

	int i,j;
	printf("\n\n\n");
	for(i=0;i<15;i++){
		for(j=0;j<15;j++){
			printf("%c ",Game_Board[i][j]);
		}
		printf("\n");
	}

}

void Put_the_words(char Game_Board[][15], char *dict[DICT_SIZE],int coord[][4]){	//putting the words

	int i=0,j,k,z=0,l=0;

	//printf("\n\n%d\n\n",DICT_SIZE );
	while(i<DICT_SIZE){													//loop < DICT_SIZE
		j=0;
		if(coord[i][j]==coord[i][j+2]){																//if just row wont change 
			Set_Horizontal(Game_Board,dict[i],coord[i][j+1],coord[i][j+3],coord[i][j],0);
			//printf("----------\n");
			//print_table(Game_Board);
		//	Set_Vertical(Game_Board,"hucre",8,12,11);
		}
		else if(coord[i][j+1]==coord[i][j+3]){														//if column wont change	
				//Set_Vertical(Game_Board,"alveol",0,5,4);
				Set_Vertical(Game_Board,dict[i],coord[i][j],coord[i][j+2],coord[i][j+1],0);
				//printf("+++++++++\n");
				//print_table(Game_Board);
}		

		else if ((coord[i][j+1]!=coord[i][j+3]) && (coord[i][j]!=coord[i][j+2])) {					// if column and row will change
			//Set_Diagonal(Game_Board,"dolasim",14,14,7,7);
			Set_Diagonal(Game_Board,dict[i],coord[i][j],coord[i][j+1],coord[i][j+2],coord[i][j+3],0);
			//printf("***********\n");
			//print_table(Game_Board);
		}

	i++;	
}
	print_table(Game_Board);							//printing first table in here
	Till_the_End(Game_Board,dict,coord);				//after that we will go to till_the_end func

}


void Set_Horizontal (char Game_Board[][15],char *dict, int x,int y,int cnst, int flag){		//if flag is 0 put the dict's words.
		//printf("-----------%s\n",dict );													//but if flag is 1 this time usimg the user's input
		int i,j;
		if( x>y){                                                                         // this is for left to right or right to left               1. DIRECTION

			for(i=0;dict[i] >= 97 && dict[i] <= 122;i++){                                // loop is bordered with lower-case words.

				if (flag == 0){

				Game_Board[cnst][x] = dict[i];
				x--;
				}
				else if(flag == 1){
				Game_Board[cnst][x] = dict[i]-32;									//-32 for doing upper-case from lower case
				x--;
				
				}
			}

		}
		else{												//SECOND DIRECTION		
			 for(i=0;dict[i] >= 97 && dict[i] <= 122;i++){

				if(flag == 0){												//same things with previous statement

				Game_Board[cnst][x] = dict[i];
				x++;
				}
				else if(flag == 1){
				Game_Board[cnst][x] = dict[i]-32;
				x++;

				}
			}
		}



}

void Set_Vertical (char Game_Board[][15],char *dict, int x,int y,int cnst, int flag){  // same things with previous statement
//printf("-----------%s\n",dict );

		int i,j;
		//printf("----%s\n",dict );	
			
		if( x>y){								//THIRD DIRECTION

			for(i=0;dict[i] >= 97 && dict[i] <= 122;i++){

				
				if(flag==0){

				Game_Board[x][cnst] = dict[i];
				x--;
				}
				else if(flag==1){
					
				Game_Board[x][cnst] = dict[i] -32;
				x--;
				}

			}

		}
		else{								//FOURTH DIRECTION
			 for(i=0; dict[i] >= 97 && dict[i] <= 122;i++){
				//printf("%c\n",dict[i] );
				if(flag == 0){

				Game_Board[x][cnst] = dict[i];
				x++;
				}
				else if(flag == 1){
				Game_Board[x][cnst] = dict[i]-32;
				x++;

				}
			}
		}



}

void Set_Diagonal (char Game_Board[][15],char *dict, int x1,int y1, int x2, int y2, int flag){
//printf("-----------%s\n",dict );

		int i;
		if( x2>x1 && y2>y1){          //FIFTH DIRECTION

			for(i=0;dict[i] >= 97 && dict[i] <= 122;i++){

				
				if(flag == 0){

				Game_Board[x1][y1] = dict[i];
				x1++;
				y1++;
				}
				else if(flag == 1){
				Game_Board[x1][y1] = dict[i]-32;
				x1++;
				y1++;
				}
			}

		}
		else if( x2<x1 && y2<y1){	//SIXTH DIRECTION

			for(i=0;dict[i] >= 97 && dict[i] <= 122;i++){

				if(flag == 0){

				Game_Board[x1][y1] = dict[i];
				x1--;
				y1--;
				}
				else if(flag == 1){
				Game_Board[x1][y1] = dict[i]-32;
				x1--;
				y1--;

				}
			}
		}
		else if( x2>x1 && y2<y1){ //SEVENTH DIRECTION

			for(i=0;dict[i] >= 97 && dict[i] <= 122;i++){

				if(flag == 0){

				Game_Board[x1][y1] = dict[i];
				x1++;
				y1--;
				}
				else if(flag == 1){
				Game_Board[x1][y1] = dict[i]-32;
				x1++;
				y1--;

				}
			}
		}	
		else if( x2<x1 && y2>y1){	//EIGHTH DIRECTION

			for(i=0;dict[i] >= 97 && dict[i] <= 122;i++){

				if(flag == 0){

				Game_Board[x1][y1] = dict[i];
				x1--;
				y1++;
				}
				else if(flag == 1){
				Game_Board[x1][y1] = dict[i]-32;
				x1--;
				y1++;	
				}
			}
		}	
}



void Till_the_End (char Game_Board[][15],char *dict[DICT_SIZE],int coord[][4]){


int game=0,i=0,j=0,counter=0,temp,check =0;
char input [DICT_SIZE];
int x,y;
	while(game<DICT_SIZE){
		printf("Please enter the word that you found\n");
		scanf(" %[^\n]",input);            //takin line from user
		if(input[0] =='e' && input[1] == 'x' && input[2] == 'i' && input[3] == 't' && input[4] == ' ' &&
			input[5] == 'g' && input[6] == 'a' && input[7] == 'm' && input[8] == 'e' )   // checking for exiting the game
			break;
		printf("(x y)Please enter first character's pozition or last character's pozition \n");
		scanf("%d %d",&x,&y);                       // taking pozitions
		


		while(input[i] >= 97 && input[i] <= 122 || input[i] != '\0'){
			//printf("%c\n",input[i]);
			counter++;										//for size of input
			i++;
		}
		
	//printf("%d\n",counter );
	for(i=0 ; i < DICT_SIZE ; i++){
		for(j=0; j<counter ; j++){							//checking the dict array 
			if(input[j]==dict[i][j]){
				//printf("%c\n",input[j]);
				check++;
			}
			
			if(check == counter){

				temp = i;
				j=0;
				if(coord[temp][j]==coord[temp][j+2]){
				//printf("Set_Horizontal\n");
				if(Game_Board[coord[temp][j]][coord[temp][j+1]] >=97 ){ //if it is down case then function calls

				Set_Horizontal(Game_Board,dict[temp],coord[temp][j+1],coord[temp][j+3],coord[temp][j],1);
				game++;												//and game++
				}
				//printf("----------\n");
				//print_table(Game_Board);
				//	Set_Vertical(Game_Board,"hucre",8,12,11);
				//game++;
				//printf("%d\n",game );
				}
				else if(coord[temp][j+1]==coord[temp][j+3]){
					//printf("Set_Vertical\n");
					//Set_Vertical(Game_Board,"alveol",0,5,4);
					if(Game_Board[coord[temp][j]][coord[temp][j+1]] >=97 ){

					Set_Vertical(Game_Board,dict[temp],coord[temp][j],coord[temp][j+2],coord[temp][j+1],1);
					game++;
					}
					//printf("+++++++++\n");
					//print_table(Game_Board);
					//printf("%d\n",game );
				}		

				else if ((coord[temp][j+1]!=coord[temp][j+3]) && (coord[temp][j]!=coord[temp][j+2])) {
					//printf("Set_Horizontal\n");
					//Set_Diagonal(Game_Board,"dolasim",14,14,7,7);
					if(Game_Board[coord[temp][j]][coord[temp][j+1]] >=97 ){

					Set_Diagonal(Game_Board,dict[temp],coord[temp][j],coord[temp][j+1],coord[temp][j+2],coord[temp][j+3],1);
					game++;
					}
					//printf("***********\n");
					//print_table(Game_Board);
					//printf("%d\n",game );		
				}
				printf("%d\n",game );
			}
				 
					
		}
		check=0;
		j=0;
	}
		j=0;
		

		temp =0;
		i =0,j=0;
		print_table(Game_Board);
		counter =0;
		

	}
}



